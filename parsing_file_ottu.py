import json
import re
import argparse
from prettytable import PrettyTable
from datetime import date


def remove_repetition(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


if __name__ == "__main__":
    """we can here parse your file to get the server, transaction, currency, and value"""
    """you only need to add your file name and path in the command line"""
    """Example: >> python parsing_file.py  --file ../example.txt """

    # to show my data in a table
    my_table = PrettyTable()
    my_table.field_names = ["Server", "Transaction", "Currency", "Volume"]

    # add argument in the command line
    parser = argparse.ArgumentParser(description="insert your file path, please")
    parser.add_argument(
        "--file", type=str, required=True, help="enter your file name with path"
    )
    args = parser.parse_args()

    file_path = args.file
    final_dicts = []

    pattern_dicts1 = r"\{\n.+?\n.+?\n.+?\n.+?\n.+?\n.+?\n\}"
    pattern_dicts2 = r"\{\n.+?\n.+?\n.+?\n.+?\n.+?\n.+?\n.+?\n\}"
    pattern_dicts3 = r"\{\n.+?\n.+?\n.+?\n.+?\n.+?\n.+?\n.+?\n.+?\n\}"
    with open(file_path) as file:
        my_data = file.read()
        # find all dictionaries in the file
        dicts = (
            re.findall(pattern_dicts1, my_data)
            + re.findall(pattern_dicts2, my_data)
            + re.findall(pattern_dicts3, my_data)
        )

    for dict in dicts:
        # make the dictionaries as json data and filter from the bad data
        dict_json = json.loads(dict)
        if (
            "Server name:" in dict_json["msg"][0]
            or "Server name:" in dict_json["msg"][1]
            or "Server name:" in dict_json["msg"][2]
        ):
            final_dicts.append(dict_json)
    # prepare the lists for the the final table
    servers_list = []
    transaction_list = []
    currency_list = []
    value_list = []
    for dict in final_dicts:
        values = []
        currencies = []
        transactions = []
        servers = []
        for str in dict["msg"]:
            if "Server name:" in str:
                servers.append(str[13:])
            if "transactions" in str:
                transaction = re.findall(r"[0-9]+", str)
                transactions.append(transaction[0])
            # every server/transaction may will have more than one value and currency
            if ("currency_code" in str) and ("value" in str):
                currency_index = str.index("currency_code")
                currency = str[currency_index + 17 : currency_index + 20]
                value_pattern = r"([\d.]*\d+)"
                value = re.findall(value_pattern, str)
                # insert them into the first list
                values.append(value[0])
                currencies.append(currency)
        # insert all lists to the second list
        value_list.append(values)
        currency_list.append(currencies)
        transaction_list.append(transactions[0])
        servers_list.append(servers[0])

    # loop on two lists at the same time
    for index, server in enumerate(servers_list):
        for i, (currency, value) in enumerate(
            zip(currency_list[index], value_list[index])
        ):
            # insert data into the tables
            if not transaction_list[index]:
                # my_table.add_row([server, "", currency, value])
                pass
            else:
                if i == 0:
                    my_table.add_row([server, transaction_list[index], currency, value])
                else:
                    my_table.add_row(["", "", currency, value])

        # some time we don't have values for this transaction
        if not list(zip(currency_list[index], value_list[index])):
            if not transaction_list[index]:
                # my_table.add_row([server, "", "", ""])
                pass
            else:
                my_table.add_row([server, transaction_list[index], "", ""])

    # extract the table as excel file
    today = date.today()
    with open(f"{file_path[:-4]}-{today}.xls", "w", newline="") as f_output:
        f_output.write(my_table.get_csv_string())
