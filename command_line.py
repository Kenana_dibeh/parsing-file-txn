# Import the library
import argparse
import socket
import ast


if __name__ == "__main__":
    # def argsParseing():
    """this script have many arguments to use them in command line"""
    """the first arguments is [domain] with validation and it is requierd """
    """the other two arguments is [arg_1] [arg_2] it is optional, not requierd with default value"""
    """the other  arguments is [you can insert any keys with values] to get them in key_pairs dectionery """

    parser = argparse.ArgumentParser(
        description="you have many arguments, you can edit them"
    )
    # here: you can change the arguments names and the arguments default values
    first_default = "empty_default"
    second_default = "empty_default"

    # here is to get the arguments and then make them in dict
    class StoreDictKeyPair(argparse.Action):
        def __call__(self, parser, namespace, values, option_string=None):
            my_dict = {}
            for kv in values.split(","):
                k, v = kv.split("=")
                my_dict[k] = v
            setattr(namespace, self.dest, my_dict)

    parser.add_argument("--domain", type=str, required=True, help="enter your domin")
    parser.add_argument(
        f"--arg_1",
        type=str,
        default=f"{first_default}",
        help=f"this is your first optional argument arg_1",
    )
    parser.add_argument(
        f"--arg_2",
        type=str,
        default=f"{second_default}",
        help=f"this is your second optional argument arg_2",
    )
    parser.add_argument(
        "key_pairs",
        action=StoreDictKeyPair,
        metavar="KEY1=VAL1,KEY2=VAL3,...",
        help="you can insert many arguments to be dict",
    )

    args = parser.parse_args()

    # domain validation
    if args.domain:
        try:
            socket.gethostbyname(args.domain.strip())
            print(
                f"Hello, we got the domain = {args.domain}  arg_1= {args.arg_1} arg_2= {args.arg_2} key_pairs = {args.key_pairs} "
            )
        except socket.gaierror:
            print("please enter your correct domain  ", args.domain)

# Example command line >> python first.py --domain Kuwaitnet.com  --arg_1 kenana --arg_2 dibeh  x=1,y=2,z=3
