import re
import argparse
from prettytable import PrettyTable


def remove_repetition(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


if __name__ == "__main__":
    """we can here parse your log file to get the servers names and another info"""
    """you only need to add your file name and path in the command line"""
    """Example: >> python parsing_file.py  --file ../example.txt """

    # to show my data in a table
    my_table = PrettyTable()
    my_table.field_names = ["ip", "Server Name", "Branch", "Last Commit", "Last login"]

    # add argumant in the command line
    parser = argparse.ArgumentParser(description="insert your file path, please")
    parser.add_argument(
        "--file", type=str, required=True, help="enter your file name with path"
    )
    args = parser.parse_args()

    file_path = args.file
    final_server_names = []
    pattern_branch = r"git:\(\S*"
    pattern_servers = r"\[\S*.ottu...."

    with open(file_path) as file:
        my_data = file.read()
        # here we get the servernames and branch without formating by RE
        branch = re.findall(pattern_branch, my_data)
        servers_names = re.findall(pattern_servers, my_data)

    # formate the branch and server names
    branch = remove_repetition(branch)
    branch = branch[0][5:-1]

    servers_names = remove_repetition(servers_names)
    for server in servers_names:
        server = server[1:]
        final_server_names.append(server)

    # throw the server names array we can get the commit and last login for each server
    for server in final_server_names:
        pattern_commit = r"\[+${server_name}+\S\s=>\s{\n\s*\S*\s\[\n\s*\"ref\S\s\S*"

        commit_result = re.findall(
            r"\[" + server + '\S\s=>\s{\n\s*\S*\s\[\n\s*"ref\S\s\S*',
            my_data,
            re.IGNORECASE,
        )
        commit = commit_result[0][-6:-1]

        login_date_result = re.findall(
            r""
            + server
            + '\S\s=>\s{\n\s*\S*\s\S\s*"ottuback\s\S*\s*\S*\s\S*\s\S*\s\S*\s\S*\s\S*',
            my_data,
            re.IGNORECASE,
        )
        login_date = login_date_result[0][-17:]
        # showing the final data in a table

        my_table.add_row([None, server, branch, commit, login_date])

    print(my_table)
